﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PJ64Savestater
{
    public class InputControl
    {
        public InputKeys? InputKey { get; set; } = null;
        public int? KeyIndex { get; set; } = null;
        public object Value { get; set; } = null;

        public static bool operator ==(InputControl left, InputControl right)
        {
            return left.InputKey == right.InputKey && left.KeyIndex == right.KeyIndex && ((left.Value == null && right.Value == null) || (left.Value != null && right.Value != null && left.Value.Equals(right.Value)));
        }

        public static bool operator !=(InputControl left, InputControl right)
        {
            return !(left == right);
        }

        public override bool Equals(object obj)
        {
            var control = obj as InputControl;
            return control != null &&
                   EqualityComparer<InputKeys?>.Default.Equals(InputKey, control.InputKey) &&
                   EqualityComparer<int?>.Default.Equals(KeyIndex, control.KeyIndex) &&
                   EqualityComparer<object>.Default.Equals(Value, control.Value);
        }

        public override int GetHashCode()
        {
            var hashCode = 2070896532;
            hashCode = hashCode * -1521134295 + EqualityComparer<InputKeys?>.Default.GetHashCode(InputKey);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(KeyIndex);
            hashCode = hashCode * -1521134295 + EqualityComparer<object>.Default.GetHashCode(Value);
            return hashCode;
        }
    }
}
