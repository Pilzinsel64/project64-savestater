﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PJ64Savestater
{
    public enum InputKeys : byte
    {
        // Uses Index

        Buttons,                 // Button index       'Default: False
        AccelerationSliders,     // ASlider index      'Default: 0
        ForceSliders,            // FSlider index      'Default: 0
        PointOfViewControllers,  // Point index        'Default: -1
        Sliders,                 // Slider index       'Default: 0
        VelocitySliders,         // VSlider index      'Default: 0

        // Don't uses Index

        AccelerationX,           // AX                 'Default: 0
        AccelerationY,           // AY                 'Default: 0
        AccelerationZ,           // AZ                 'Default: 0
        AngularAccelerationX,    // AAX                'Default: 0
        AngularAccelerationY,    // AAY                'Default: 0
        AngularAccelerationZ,    // AAZ                'Default: 0
        ForceX,                  // FX                 'Default: 0
        ForceY,                  // FY                 'Default: 0
        ForceZ,                  // FZ                 'Default: 0
        RotationX,               // RX                 'Default: 0
        RotationY,               // RY                 'Default: 0
        RotationZ,               // RZ                 'Default: 0
        TorqueX,                 // TX                 'Default: 0
        TorqueY,                 // TY                 'Default: 0
        TorqueZ,                 // TZ                 'Default: 0
        VelocityX,               // VX                 'Default: 0
        VelocityY,               // VY                 'Default: 0
        VelocityZ,               // VZ                 'Default: 0
        X,                       // X                  'Default: 0
        Y,                       // Y                  'Default: 0
        Z                        // Z                  'Default: 0
    }
}
