﻿namespace PJ64Savestater
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_Pads = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton_LoadPads = new Telerik.WinControls.UI.RadButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radTextBox_LoadSavestate = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_CreateSavestate = new Telerik.WinControls.UI.RadTextBox();
            this.radButton_LoadProfile = new Telerik.WinControls.UI.RadButton();
            this.radButton_SaveProfile = new Telerik.WinControls.UI.RadButton();
            this.radButton_StartStopListening = new Telerik.WinControls.UI.RadButton();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Pads)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_LoadPads)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_LoadSavestate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CreateSavestate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_LoadProfile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SaveProfile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_StartStopListening)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Image = global::PJ64Savestater.Properties.Resources.icons8_game_controller_16px;
            resources.ApplyResources(this.radLabel1, "radLabel1");
            this.radLabel1.Name = "radLabel1";
            // 
            // radDropDownList_Pads
            // 
            this.radDropDownList_Pads.DropDownAnimationEnabled = true;
            this.radDropDownList_Pads.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            resources.ApplyResources(this.radDropDownList_Pads, "radDropDownList_Pads");
            this.radDropDownList_Pads.Name = "radDropDownList_Pads";
            this.radDropDownList_Pads.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.radDropDownList1_SelectedIndexChanged);
            // 
            // radButton_LoadPads
            // 
            this.radButton_LoadPads.Image = global::PJ64Savestater.Properties.Resources.icons8_refresh_16px;
            resources.ApplyResources(this.radButton_LoadPads, "radButton_LoadPads");
            this.radButton_LoadPads.Name = "radButton_LoadPads";
            this.radButton_LoadPads.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.radTextBox_LoadSavestate);
            this.panel1.Controls.Add(this.radTextBox_CreateSavestate);
            this.panel1.Controls.Add(this.radButton_LoadProfile);
            this.panel1.Controls.Add(this.radButton_SaveProfile);
            this.panel1.Controls.Add(this.radButton_StartStopListening);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.radButton_LoadPads);
            this.panel1.Controls.Add(this.radDropDownList_Pads);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // radTextBox_LoadSavestate
            // 
            resources.ApplyResources(this.radTextBox_LoadSavestate, "radTextBox_LoadSavestate");
            this.radTextBox_LoadSavestate.Name = "radTextBox_LoadSavestate";
            this.radTextBox_LoadSavestate.TextChanged += new System.EventHandler(this.radTextBox2_TextChanged);
            this.radTextBox_LoadSavestate.Click += new System.EventHandler(this.RadTextBox_Savestate_Clicked);
            this.radTextBox_LoadSavestate.LostFocus += new System.EventHandler(this.RadTextBox_Savestate_LostFocus);
            // 
            // radTextBox_CreateSavestate
            // 
            resources.ApplyResources(this.radTextBox_CreateSavestate, "radTextBox_CreateSavestate");
            this.radTextBox_CreateSavestate.Name = "radTextBox_CreateSavestate";
            this.radTextBox_CreateSavestate.TextChanged += new System.EventHandler(this.radTextBox1_TextChanged);
            this.radTextBox_CreateSavestate.Click += new System.EventHandler(this.RadTextBox_Savestate_Clicked);
            this.radTextBox_CreateSavestate.LostFocus += new System.EventHandler(this.RadTextBox_Savestate_LostFocus);
            // 
            // radButton_LoadProfile
            // 
            this.radButton_LoadProfile.Image = global::PJ64Savestater.Properties.Resources.icons8_opened_folder_16px;
            resources.ApplyResources(this.radButton_LoadProfile, "radButton_LoadProfile");
            this.radButton_LoadProfile.Name = "radButton_LoadProfile";
            this.radButton_LoadProfile.Click += new System.EventHandler(this.radButton4_Click);
            // 
            // radButton_SaveProfile
            // 
            this.radButton_SaveProfile.Image = global::PJ64Savestater.Properties.Resources.icons8_save_16px;
            resources.ApplyResources(this.radButton_SaveProfile, "radButton_SaveProfile");
            this.radButton_SaveProfile.Name = "radButton_SaveProfile";
            this.radButton_SaveProfile.Click += new System.EventHandler(this.radButton3_Click);
            // 
            // radButton_StartStopListening
            // 
            this.radButton_StartStopListening.Image = global::PJ64Savestater.Properties.Resources.icons8_play_button_circled_16px;
            resources.ApplyResources(this.radButton_StartStopListening, "radButton_StartStopListening");
            this.radButton_StartStopListening.Name = "radButton_StartStopListening";
            this.radButton_StartStopListening.Click += new System.EventHandler(this.radButton_StartStopListening_Click);
            // 
            // radLabel3
            // 
            resources.ApplyResources(this.radLabel3, "radLabel3");
            this.radLabel3.Name = "radLabel3";
            // 
            // radLabel2
            // 
            resources.ApplyResources(this.radLabel2, "radLabel2");
            this.radLabel2.Name = "radLabel2";
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "MainForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Pads)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_LoadPads)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_LoadSavestate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CreateSavestate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_LoadProfile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SaveProfile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_StartStopListening)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Pads;
        private Telerik.WinControls.UI.RadButton radButton_LoadPads;
        private Panel panel1;
        private Telerik.WinControls.UI.RadButton radButton_StartStopListening;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadButton radButton_LoadProfile;
        private Telerik.WinControls.UI.RadButton radButton_SaveProfile;
        private Telerik.WinControls.UI.RadTextBox radTextBox_LoadSavestate;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CreateSavestate;
    }
}
