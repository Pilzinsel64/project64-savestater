﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PJ64Savestater
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            VisualThemeHelper.SetVisualTheme();
            //Application.SetDefaultFont(new Font(Control.DefaultFont.FontFamily, 8.25f)); // Kein Effekt?
            Application.Run(new MainForm());
        }
    }
}
