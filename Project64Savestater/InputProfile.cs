﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PJ64Savestater
{
    public class InputProfile
    {
        public string ProfileCode { get; set; }
        public Dictionary<string, InputControl> Controls { get; } = new Dictionary<string, InputControl>();

        private byte[] _ControllerInstanceGuid = Array.Empty<byte>();

        [JsonConstructor]
        public InputProfile()
        {
        }

        public InputProfile(string profileCode)
        {
            ProfileCode = profileCode;
        }

        public Guid ControllerInstanceGuid
        {
            get
            {
                if (_ControllerInstanceGuid.Count() == 0)
                    return default;
                return new Guid(_ControllerInstanceGuid);
            }
            set
            {
                _ControllerInstanceGuid = value.ToByteArray();
            }
        }

        public InputControl this[string name]
        {
            get
            {
                if (!Controls.ContainsKey(name))
                    Controls.Add(name, new InputControl());
                return Controls[name];
            }
            set
            {
                if (!Controls.ContainsKey(name))
                    Controls.Add(name, value);
                else
                    Controls[name] = value;
            }
        }

        public void ClearProfile()
        {
            Controls.Clear();
        }

        public void Save(string fileName)
        {
            File.WriteAllText(fileName, JObject.FromObject(this, GetJsonSerializer()).ToString());
        }

        public static InputProfile Load(string fileName)
        {
            return (InputProfile)JObject.Parse(File.ReadAllText(fileName)).ToObject(typeof(InputProfile), GetJsonSerializer());
        }

        private static JsonSerializer GetJsonSerializer()
        {
            var serializer = JsonSerializer.CreateDefault();
            serializer.TypeNameHandling = TypeNameHandling.Auto;
            return serializer;
        }
    }
}
