﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PJ64Savestater
{
    internal static class Globals
    {
        public static bool CompareTwoByteArrays(byte[] arr1, byte[] arr2)
        {
            if (arr2.Count() != arr1.Count())
                return false;
            for (int i = 0, loopTo = arr1.Count() - 1; i <= loopTo; i++)
            {
                if (arr1[i] != arr2[i])
                    return false;
            }

            return true;
        }
    }
}
