﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls;

namespace PJ64Savestater
{
    internal static class VisualThemeHelper
    {
        public static void SetVisualTheme()
        {
            RadThemeComponentBase? setTheme = null;

            if (!WindowsSettings.AppsUseLightTheme)
                setTheme = new Telerik.WinControls.Themes.FluentDarkTheme();
            setTheme ??= new Telerik.WinControls.Themes.FluentTheme();

            // Set theme
            ThemeResolutionService.ApplicationThemeName = setTheme.ThemeName;
        }
    }
}
